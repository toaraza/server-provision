#!/usr/bin/env bash

# Setup locale
cat <<EOF >> /etc/default/locale 
LANG="en_US.UTF-8"
LC_ALL="en_US.UTF-8"
LANGUAGE="en_US.UTF-8"
EOF

# Setup date and time
echo 'Europe/Paris' > /etc/timezone
dpkg-reconfigure --frontend noninteractive tzdata

apt-get update
apt-get install -y \
  language-pack-en \
  git \
  zsh \
  software-properties-common \

. /etc/default/locale

apt-add-repository ppa:ansible/ansible
apt-get update
apt-get install -y ansible

cat <<EOF > /etc/ansible/hosts  
[web]
localhost ansible_connection=local
EOF

useradd -m -s /bin/bash -G sudo manager

echo "manager ALL=(ALL) NOPASSWD:ALL" | (EDITOR="tee -a" visudo)
