
if [ ! -x "$(command -v mkpasswd)" ]; then
  apt-get install -y makepasswd whois
fi

username="deployer"
password=$(makepasswd --chars 10)

useradd \
  -p `mkpasswd $password` \
  -d /home/$username \
  -m \
  -s /bin/bash \
  $username
echo "User $username created"
echo "password = $password"
